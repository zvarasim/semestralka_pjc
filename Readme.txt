Program na ortogonalizaci a ortogonalizaci báze vektorů v prostoru R^n.
    Šimon Zvára (zvarasim)
    SIT - FEL - ČVUT
    3. semestr
    Programování v C++

Hash commitu:
3b3d6d431282af7f50144c55e5ac8082a82f43e4

Zadání:
    Program přijme na vstupu hodnotu dimenze a bázi vektorů v této dimenzi.
    Program umožní ortogonalizaci, normalizaci a ortonormalizaci této báze na
    jednom nebo více vláknech, popřípadě vypíše hlášku, že se nejedná o platnou bázi.
    Program bude číst báze ze standardního a souborového vstupu.

Popis implementace:
    K implementaci jsem použil Gram-Schmidtův algoritmus známý z Lineární algebry.
    Pro podporu více vláken jsem použil Work Crew model, kde rozdělím vektory v
    bázové matici na stejně velké části, nad kterými pracuji pararelně. U větších
    matic je zvýšení výkonu vícevláknového řešení až několikanásobné.
    Do programu lze báze psát buď manuálně nebo ze souboru. Program umí číst
    ze souboru kontinuálně, dokud nenarazí na chybu nebo konec souboru.
    Při ortogonalizaci probíhá kontrola, zda-li se skutečně jedná o platnou
    bázovou matici. Při chybě je vyhozena vyjímka ZeroVectorException.
    Program lze konfigurovat pomocí přepínačů z příkazové řádky.

Přiložená testovací data:
    small.txt - sada malých bází (50x50...10x1O)
    medium.txt - sada středních bází (100x100...50x50)
    large.txt - sada velkých bází (500x500...100x100)

Přepínače:
    -n             Normalizace báze
    -g             Ortogonalizace báze
    -f <filepath>  Načítání ze souboru
    -m             Využití více vláken
    --help         Nápověda

Příklad použití:
    semestralka -g -n -f input.txt -m       (vícevláknová ortonormalizace ze souboru)
    semestralka -g                          (pouze ortogonalizace)
    semestralka -g -n                       (ortonormalizace)

Naměřené výsledky:
    small.txt
        jedno vlákno: 55 ms
        více vláken: 70 ms    - na malých bázích je výhodnější jedno vlákno
    medium.txt
        jedno vlákno: 170 ms
        více vláken: 157 ms   - zde je již efektivnější využít více vláken
    large.txt
        jedno vlákno: 7935 ms
        více vláken: 2750 ms  - na velkých maticích se více vláken vyplatí

Prostředí:
    IDE: JetBrains CLion 2020.3
    Model Name:	MacBook Pro (13-inch, 2018, Four Thunderbolt 3 Ports)
    OS: MacOS Big Sur 11.1
    Model Identifier:	MacBookPro15,2
    Processor Name:	Quad-Core Intel Core i5
    Processor Speed:	2,3 GHz
    Number of Processors:	1
    Total Number of Cores:	4 (8 threads)
    L2 Cache (per Core):	256 KB
    L3 Cache:	6 MB
    Hyper-Threading Technology:	Enabled
    Memory:	8 GB 2133 MHz LPDDR3
