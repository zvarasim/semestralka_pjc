//
// Created by Šimon Zvára on 07.01.2021.
//

#ifndef SEMESTRALKA_VECTOR_HPP
#define SEMESTRALKA_VECTOR_HPP

#include <vector>

class Vector {
private:
public:
    explicit Vector(int size);
    double norm() const;
    void normalize();
    double operator*(const Vector& rhs) const;
    Vector operator*(const double& value) const;
    Vector operator+(const Vector& rhs) const;
    Vector operator-(const Vector& rhs) const;
    int size;
    std::vector<double> elements;
};

class ZeroVectorException: std::exception {};

#endif //SEMESTRALKA_VECTOR_HPP
