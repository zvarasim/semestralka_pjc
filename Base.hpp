//
// Created by Šimon Zvára on 07.01.2021.
//

#ifndef SEMESTRALKA_BASE_HPP
#define SEMESTRALKA_BASE_HPP

#include "vector"
#include "Vector.hpp"

class Base {
public:
    explicit Base(int size);
    void validateResult() const;
    void ortogonalize();
    void ortogonalizeMulti();
    void normalize();
    void normalizeMulti();
    bool isOrtogonal() const;
    bool isNormalized() const;
    std::vector<Vector> vektors;
private:
    int size;
};

#endif //SEMESTRALKA_BASE_HPP
