#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include "Base.hpp"
#include "chrono"
#include "cmath"

using namespace std;

template <typename TimePoint>
chrono::milliseconds to_ms(TimePoint tp) {
    return chrono::duration_cast<chrono::milliseconds>(tp);
}

ostream& operator<<(ostream &os, Vector const &vector) {
    os << "( ";
    for(int e = 0; e < vector.size; e++) {
        if(e != 0) {
            os << " ; ";
        }
        auto value = round( vector.elements[e] * 1000.0 ) / 1000.0;
        os << setw(7) << value << setw(0);
    }
    os << " )";
    return os;
}

ostream& operator<<(ostream &os, Base const &base) {
    for(const auto& vector : base.vektors) {
        os << vector << endl;
    }
    return os;
}

bool normalize = false;
bool ortogonalize = false;
bool multithreaded = false;

/**
 * Načte bázi ze standardního vstupu a vykoná vybrané akce.
 * @return Vrací 1, pokud dojde k chybě, jinak 0.
 */

int fromCin() {

    int dimension;

    cout << "Zvolte dimenzi N: ";

    while (true) {
        cin >> dimension;
        if (!cin) {
            cin.clear();
            cin.ignore(10000, '\n');
            cout << "Neplatný vstup! Zkuste znovu:" << endl;
        } else {
            break;
        }
    }
    cin.clear();
    cin.ignore(10000, '\n');

    Base base(dimension);

    for (int v = 0; v < dimension; v++) {

        cout << "Zadejte prvky vektoru " << v+1 << "/" << dimension << " ve tvaru \"1 3 4 5\": ";

        string line;
        getline(cin, line);

        stringstream ss(line);

        for (int e = 0; e < dimension; e++) {
            double number;
            ss >> number;
            base.vektors[v].elements[e] = number;
            if(ss.fail()) {
                cout << "Neplatný vstup! Zkuste znovu:" << endl;
                v--;
                break;
            }
        }
    }

    auto startTime = chrono::high_resolution_clock::now();

    try {
        if (ortogonalize) {
            multithreaded ? base.ortogonalizeMulti() : base.ortogonalize();
        }

        if (normalize) {
            multithreaded ? base.normalizeMulti() : base.normalize();
        }

        base.validateResult();

        cout << "Vaše výsledná báze:" << endl;
        cout << base;

    } catch (ZeroVectorException& e) {
        cout << "Toto není platná báze, vektory jsou lineárně závislé!" << endl;
    }

    auto endTime = std::chrono::high_resolution_clock::now();
    std::cout << "Čas běhu: " << to_ms(endTime - startTime).count() << " ms" << endl;

    return 0;
}

/**
 * Načte bázi ze souborového vstupu a vykoná vybrané akce.
 * @return Vrací 1, pokud dojde k chybě, jinak 0.
 */

int fromFile(const string& filePath) {

    ifstream infile(filePath);

    if(infile.fail()) {
        cout << "Soubor \"" << filePath << "\" nebyl nalezen!";
        return 1;
    }

    cout << "------------------------------" << endl;

    auto startTime = chrono::high_resolution_clock::now();

    while (!infile.eof()) {

        int dimension;
        infile >> dimension;
        infile.clear();
        infile.ignore(10000, '\n');

        Base base(dimension);

        for (int v = 0; v < dimension; v++) {

            string line;
            getline(infile, line);
            stringstream ss(line);

            for (int e = 0; e < dimension; e++) {
                double number;
                ss >> number;
                base.vektors[v].elements[e] = number;
                if (ss.fail()) {
                    cout << "Chyba v souborovém vstupu!" << endl;
                    return 1;
                }
            }
        }

        try {
            if (ortogonalize) {
                multithreaded ? base.ortogonalizeMulti() : base.ortogonalize();
            }

            if (normalize) {
                multithreaded ? base.normalizeMulti() : base.normalize();
            }

            base.validateResult();

            cout << base;

        } catch (ZeroVectorException &e) {
            cout << "Toto není platná báze, vektory jsou lineárně závislé!" << endl;
        }
        cout << "------------------------------" << endl;
    }

    auto endTime = std::chrono::high_resolution_clock::now();
    std::cout << "Čas běhu: " << to_ms(endTime - startTime).count() << " ms" << endl;

    return 0;
}

int main(int argc, char *argv[]) {

    bool fileInput = false;
    string filePath;

    for(int i = 1; i < argc; i++) {
        string command = argv[i];
        if(command == "-n") {
            normalize = true;
        } else if (command == "-g") {
            ortogonalize = true;
        } else if (command == "-f") {
            if (++i < argc) {
                fileInput = true;
                filePath = argv[i];
            } else {
                cerr << "Chybí cesta k souboru!";
                return 1;
            }
        } else if (command == "-m") {
            multithreaded = true;
        } else if (command == "--help") {
            cout << "Použití:" << endl
            << setw(15) << left << "-n" << setw(0) << "Normalizace báze" << endl
            << setw(15) << left << "-g" << setw(0) << "Ortogonalizace báze" << endl
            << setw(15) << left << "-f <filepath>" << setw(0) << "Načítání ze souboru" << endl
            << setw(15) << left << "-m" << setw(0) << "Využití více vláken" << endl
            << setw(15) << left << "--help" << setw(0) << "Nápověda";
            return 0;
        } else {
            cerr << "Neznámý přepínač: " + command;
            return 1;
        }
    }

    if(fileInput) {
        return fromFile(filePath);
    } else {
        return fromCin();
    }
}
