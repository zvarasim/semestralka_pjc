//
// Created by Šimon Zvára on 07.01.2021.
//

#include <cmath>
#include "Vector.hpp"

Vector::Vector(int size) : size(size), elements(size) {}

/**
 * Norma (velikost) vektoru vypočtená pomocí Eukleidova vzorce.
 * @return Norma vektoru.
 */

double Vector::norm() const {
    double toSqrt = 0;
    for(int e = 0; e < size; e++) {
        toSqrt += pow(elements[e], 2);
    }
    return sqrt(toSqrt);
}

/**
 * Lineární součin dvou vektorů.
 * @param rhs Vector na pravé straně.
 * @return Hodnota lineárního součinu.
 */

double Vector::operator*(const Vector& rhs) const {
    double result = 0;
    for(int e = 0; e < size; e++) {
        result += elements[e] * rhs.elements[e];
    }
    return result;
}

/**
 * Normalizuje daný vektor. (vydělí jednotlivé prvky normou vektoru)
 */

void Vector::normalize() {
    double norm = this->norm();
    for(int e = 0; e < size; e++) {
        elements[e] /= norm;
    }
}

/**
 * Vynásobí vektor skalárem.
 * @param value Hodnota skaláru.
 * @return Výsledný vektor.
 */

Vector Vector::operator*(const double &value) const {
    Vector result(size);

    for(int e = 0; e < size; e++) {
        result.elements[e] = elements[e] * value;
    }

    return result;
}

/**
 * Sečte dva vektory.
 * @param rhs Vektor pravé strany.
 * @return Výsledný vektor.
 */

Vector Vector::operator+(const Vector& rhs) const {
    Vector result(size);

    for(int e = 0; e < size; e++) {
        result.elements[e] = elements[e] + rhs.elements[e];
    }

    return result;
}

/**
 * Odečte dva vektory.
 * @param rhs Vektor pravé strany
 * @return Výsledný vektor.
 */

Vector Vector::operator-(const Vector& rhs) const {
    Vector result(size);

    for(int e = 0; e < size; e++) {
        result.elements[e] = elements[e] - rhs.elements[e];
    }

    return result;
}
