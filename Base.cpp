//
// Created by Šimon Zvára on 07.01.2021.
//

#include <iostream>
#include "Base.hpp"
#include "thread"
#include "cmath"
#include "mutex"

Base::Base(int size) : size(size) {
    for (int i = 0; i < size; i++) {
        vektors.emplace_back(size);
    }
}

/**
 * Ortogonalizuje vektory v bázi pomocí Gram–Schmidtova algoritmu na jednom vlákně,
 * popřípadě vyhodí vyjímku, jsou li vektory lineárně závislé.
 */

void Base::ortogonalize() {

    for(int v = 1; v < size; v++) {
        Vector& b = vektors[v];
        Vector projekce(size);

        for(int w = 0; w < v; w++) {
            Vector& u = vektors[w];
            auto A = b*u;
            auto B = u*u;
            Vector toAdd = u * (A / B);
            projekce = projekce + toAdd;
        }

        vektors[v] = b - projekce;

        if(vektors[v].norm() == 0) {
            throw ZeroVectorException();
        }
    }

}

/**
 * Ortogonalizuje vektory v bázi pomocí Gram–Schmidtova algoritmu na více vláknech,
 * popřípadě vyhodí vyjímku, jsou li vektory lineárně závislé. (Work Crew model)
 */

void Base::ortogonalizeMulti() {

    uint num_threads = std::thread::hardware_concurrency();
    std::mutex mutex;

    for(int v = 1; v < size; v++) {
        Vector& b = vektors[v];
        Vector projekce(size);

        auto thread_func = [&](int from, int to) {
            Vector toAddSum(size);
            for (int i = from; i < to; i++) {
                Vector& u = vektors[i];
                Vector toAdd = u * ((b*u) / (u*u));
                toAddSum = toAddSum + toAdd;
            }
            std::unique_lock<std::mutex> lock(mutex);   // Zámek
            projekce = projekce + toAddSum;
        };

        uint part_size = v / num_threads;
        uint mod = v % num_threads;

        std::vector<std::thread> threads;
        for (uint i = 0; i < num_threads; ++i) {
            int from = i * part_size + std::min(i, mod);
            int to = (i + 1) * part_size + std::min(i+1, mod);
            if(from != to) {
                threads.emplace_back(thread_func, from, to);
            }
        }
        for (auto& t : threads) {
            t.join();
        }

        vektors[v] = b - projekce;

        if(vektors[v].norm() == 0) {
            throw ZeroVectorException();
        }
    }

}

/**
 * Normalizuje vektory v bázi na jednom vlákně.
 */

void Base::normalize() {
    for(auto& vector: vektors) {
        vector.normalize();
    }
}

/**
 * Normalizuje vektory v bázi na více vláknech. (Work Crew model)
 */

void Base::normalizeMulti() {

    auto thread_func = [&](int from, int to) {
        for (int i = from; i < to; ++i) {
            vektors[i].normalize();
        }
    };

    uint num_threads = std::thread::hardware_concurrency();
    uint part_size = vektors.size() / num_threads;
    uint mod = vektors.size() % num_threads;

    std::vector<std::thread> threads;
    for (uint i = 0; i < num_threads; ++i) {
        threads.emplace_back(thread_func,
                             i * part_size + std::min(i, mod),
                             (i + 1) * part_size + std::min(i+1, mod)
                             );
    }
    for (auto& t : threads) {
        t.join();
    }
}

/**
 * Ověří ortogonalizu báze.
 * @return Vrací true, je-li báze ortogonální, jinak false.
 */

bool Base::isOrtogonal() const {
    for(int a = 0; a < vektors.size(); a++) {
        for(int b = 0; b < vektors.size(); b++) {
            auto product = vektors[a]*vektors[b];
            if(abs(product) > 0.01 && a != b) {
                return false;
            }
        }
    }
    return true;
}

/**
 * Ověří normalizaci báze.
 * @return Vrací true, je-li báze normalizovaná, jinak false.
 */

bool Base::isNormalized() const {
    for(const auto& vector : vektors) {
        if(abs(vector.norm() - 1) > 0.01) {
            return false;
        }
    }
    return true;
}

/**
 * Ověří, zdali báze neobsahuje nulové nebo neplatné vektory.
 * @return Vrací true, pokud báze neobsahuje žádné nulové nebo neplatné vektory, jinak false.
 */

void Base::validateResult() const {
    for(const auto& vector : vektors) {
        auto norm = vector.norm();
        if(norm == 0 || std::isnan(norm)) {
            throw ZeroVectorException();
        }
    }
}